/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "flashfat.h"
#include "kprintf.h"

#include <pspsdk.h>
#include <pspkernel.h>

#include <string.h>

int g_enable_redirection = 1;

int (* FlashFatIoOpen)(PspIoDrvFileArg *arg, char *file, int flags, SceMode mode) = NULL;
int (* FlashFatIoRemove)(PspIoDrvFileArg *arg, const char *name) = NULL;
int (* FlashFatIoMkdir)(PspIoDrvFileArg *arg, const char *name, SceMode mode) = NULL;
int (* FlashFatIoRmdir)(PspIoDrvFileArg *arg, const char *name) = NULL;
int (* FlashFatIoDopen)(PspIoDrvFileArg *arg, const char *dirname) = NULL;
int (* FlashFatIoGetstat)(PspIoDrvFileArg *arg, const char *file, SceIoStat *stat) = NULL;
int (* FlashFatIoChstat)(PspIoDrvFileArg *arg, const char *file, SceIoStat *stat, int bits) = NULL;
int (* FlashFatIoRename)(PspIoDrvFileArg *arg, const char *oldname, const char *newname) = NULL;
int (* FlashFatIoChdir)(PspIoDrvFileArg *arg, const char *dir) = NULL;

static int is_kd_directory(const char *s1)
{
    if (strlen(s1) < 3)
        return 0;

    return (tolower(s1[0]) == '/' && tolower(s1[1]) == 'k' && tolower(s1[2]) == 'd');
}

static int is_vsh_directory(const char *s1)
{
    if (strlen(s1) < 4)
        return 0;

    return (tolower(s1[0]) == '/' && tolower(s1[1]) == 'v' && tolower(s1[2]) == 's' && tolower(s1[3]) == 'h');
}

static char *fixupFilename(const char *filename)
{
    if (!g_enable_redirection)
        return filename;

    // ugh, bad practice but only affects internal buffers
    char *filename_ = (char *)filename;

    // convert /kd to /kn
    if (is_kd_directory(filename_))
    {
        filename_[2] = 'n';
    }

    // convert /vsh to /vsn
    else if (is_vsh_directory(filename_))
    {
        filename_[3] = 'n';
    }

    return filename_;
}

int FlashFatIoOpenPatched(PspIoDrvFileArg *arg, char *file, int flags, SceMode mode)
{
    kprintf("%s accessing file %s. fixed up: %s\n", __FUNCTION__, file, fixupFilename(file));
    // just fixup the filename and return original function
	int res = FlashFatIoOpen(arg, fixupFilename(file), flags, mode);
    kprintf("result: 0x%08X\n", res);
    return res;
}

int FlashFatIoRemovePatched(PspIoDrvFileArg *arg, const char *name)
{
    kprintf("%s accessing file %s. fixed up: %s\n", __FUNCTION__, name, fixupFilename(name));
    // just fixup the filename and return original function
    return FlashFatIoRemove(arg, fixupFilename(name));
}

int FlashFatIoMkdirPatched(PspIoDrvFileArg *arg, const char *name, SceMode mode)
{
    kprintf("%s accessing file %s. fixed up: %s\n", __FUNCTION__, name, fixupFilename(name));
    // just fixup the filename and return original function
    return FlashFatIoMkdir(arg, fixupFilename(name), mode);
}

int FlashFatIoRmdirPatched(PspIoDrvFileArg *arg, const char *name)
{
    kprintf("%s accessing file %s. fixed up: %s\n", __FUNCTION__, name, fixupFilename(name));
    // just fixup the filename and return original function
    return FlashFatIoRmdir(arg, fixupFilename(name));
}

int FlashFatIoDopenPatched(PspIoDrvFileArg *arg, const char *dirname)
{
    kprintf("%s accessing file %s. fixed up: %s\n", __FUNCTION__, dirname, fixupFilename(dirname));
    // just fixup the filename and return original function
    return FlashFatIoDopen(arg, fixupFilename(dirname));
}

int FlashFatIoGetstatPatched(PspIoDrvFileArg *arg, const char *file, SceIoStat *stat)
{
    kprintf("%s accessing file %s. fixed up: %s\n", __FUNCTION__, file, fixupFilename(file));
    // just fixup the filename and return original function
    int res = FlashFatIoGetstat(arg, fixupFilename(file), stat);
    kprintf("getstat res: 0x%08X\n", res);
    return res;
}

int FlashFatIoChstatPatched(PspIoDrvFileArg *arg, const char *file, SceIoStat *stat, int bits)
{
    kprintf("%s accessing file %s. fixed up: %s\n", __FUNCTION__, file, fixupFilename(file));
    // just fixup the filename and return original function
    return FlashFatIoChstat(arg, fixupFilename(file), stat, bits);
}

int FlashFatIoRenamePatched(PspIoDrvFileArg *arg, const char *oldname, const char *newname)
{
    // just fixup the filename and return original function
    return FlashFatIoRename(arg, fixupFilename(oldname), fixupFilename(newname));
}

int FlashFatIoChdirPatched(PspIoDrvFileArg *arg, const char *dir)
{
    kprintf("%s accessing file %s. fixed up: %s\n", __FUNCTION__, dir, fixupFilename(dir));
    // just fixup the filename and return original function
    return FlashFatIoChdir(arg, fixupFilename(dir));
}

int sceIoAddDrvPatched(PspIoDrv *drv)
{
    if (drv->name && strcmp(drv->name, "flashfat") == 0)
    {
        // backup original driver functions
        FlashFatIoOpen = drv->funcs->IoOpen;
        FlashFatIoRemove = drv->funcs->IoRemove;
        FlashFatIoMkdir = drv->funcs->IoMkdir;
        FlashFatIoRmdir = drv->funcs->IoRmdir;
        FlashFatIoDopen = drv->funcs->IoDopen;
        FlashFatIoGetstat = drv->funcs->IoGetstat;
        FlashFatIoChstat = drv->funcs->IoChstat;
        FlashFatIoRename = drv->funcs->IoRename;
        FlashFatIoChdir = drv->funcs->IoChdir;

        // replace driver with our functions
        drv->funcs->IoOpen = FlashFatIoOpenPatched;
        drv->funcs->IoRemove = FlashFatIoRemovePatched;
        drv->funcs->IoMkdir = FlashFatIoMkdirPatched;
        drv->funcs->IoRmdir = FlashFatIoRmdirPatched;
        drv->funcs->IoDopen = FlashFatIoDopenPatched;
        drv->funcs->IoGetstat = FlashFatIoGetstatPatched;
        drv->funcs->IoChstat = FlashFatIoChstatPatched;
        drv->funcs->IoRename = FlashFatIoRenamePatched;
        drv->funcs->IoChdir = FlashFatIoChdirPatched;
    }

	return sceIoAddDrv(drv);
}
