/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "utility.h"
#include "flashfat.h"
#include "libinfinity.h"
#include "reboot.h"
#include "psp_uart.h"
#include "kprintf.h"

#include <pspmacro.h>

#include <pspkernel.h>
#include <pspsysevent.h>
#include <pspsysmem_kernel.h>
#include <psploadcore.h>
#include <pspctrl.h>

#include <string.h>

PSP_MODULE_INFO("InfinityControl", 0x7007, 1, 0);

int (* PrologueModule)(void *modmgr_param, SceModule2 *mod) = (void *)NULL;

// bfc encryption key parts
const char g_661_1000_keys[] =
{
    0x06, 0x71, 0x54, 0x2E, 0x8B, 0xC0, 0xDF, 0xFB,
    0x08, 0xCC, 0x7F, 0x08, 0x4E, 0x33, 0x60, 0xAA,
};

const char g_661_2000_keys[] =
{
    0x89, 0x93, 0x0D, 0x83, 0x71, 0x7F, 0x02, 0xF2,
    0xC5, 0x0D, 0x06, 0x2D, 0x05, 0xA9, 0x78, 0xBA,
    0x4D, 0x82, 0xC9, 0x26, 0xA5, 0xA7, 0x35, 0x33,
    0x68, 0x63, 0xCD, 0xD5, 0x1D, 0xA1, 0x36, 0xD1,
};

const char g_661_3000_keys[] =
{
    0x43, 0x0E, 0x99, 0xA2, 0x73, 0x57, 0x37, 0x31,
    0xBF, 0x22, 0x9C, 0x5B, 0x69, 0x79, 0xB0, 0xEC,
    0xF0, 0x7B, 0x99, 0xDF, 0x21, 0x54, 0x80, 0xFA,
    0x97, 0x37, 0x2D, 0xEB, 0x02, 0x73, 0x32, 0x94,
};

const char g_661_go_keys[] =
{
    0xEA, 0xCC, 0x21, 0x4E, 0x5C, 0x1F, 0x64, 0x31,
    0x8A, 0x12, 0x20, 0x12, 0x39, 0xD5, 0x2D, 0x19,
    0x70, 0x3F, 0xC8, 0x28, 0xB1, 0x02, 0x75, 0x40,
    0x73, 0x94, 0xBA, 0xF5, 0x2A, 0x12, 0x0E, 0xA8,
};

int (* sceLflashFatfmtStartFatfmtOriginal)(int argc, char *argv[]) = NULL;

int sceLflashFatfmtStartFatfmtPatched(int argc, char *argv[])
{
    if (g_enable_redirection)
        return 0xDAEEDAEE;

    return sceLflashFatfmtStartFatfmtOriginal(argc, argv);
}

int sceSysconWriteScratchPad(u32 addr, u8 *buffer, u32 len);
int sceSysconWriteScratchPadPatched(u32 addr, u8 *buffer, u32 len)
{
    // check for sleep
    if (addr == 0xC && len == 0x4)
    {
        u32 *version = (u32 *)buffer;

        for (int i = 0; i < 0x200/4; ++i)
        {
            if (version[-i] == 0x06060110)
            {
                version[-i] = 0x06030110;
                break;
            }
        }
    }

    /* return the original function */
    return sceSysconWriteScratchPad(addr, buffer, len);
}

int PrologueModulePatched(void *modmgr_param, SceModule2 *mod)
{
    u32 text_addr = mod->text_addr;

	// modmgr_param has changed from 1.50 so I have not included the structure definition, for an updated version a re-reverse of 6.20+ modulemgr is required
	int res = PrologueModule(modmgr_param, mod);

	// If this function errors, the module is shutdown so we better check for it
	if (res >= 0)
	{
        if (strcmp(mod->modname, "sceLFatFs_Driver") == 0)
        {
            // patch nand decryption to use 6.31 scrambles
            _sw(0x3C04E370, text_addr + 0x00008E8C); // 6.61
            _sw(0x34881A7B, text_addr + 0x00008E94); // 6.61

            // patch fat driver
            REDIRECT_FUNCTION(text_addr + 0x0000AEA0, sceIoAddDrvPatched); // 6.61
            ClearCaches();
        }
        else if (strcmp(mod->modname, "sceLflashFatfmt") == 0)
    	{
            u32 addr = FindExport("sceLflashFatfmt", "LflashFatfmt", 0xB7A424A4);

            if (addr != 0)
            {
                KERNEL_HIJACK_FUNCTION(addr, sceLflashFatfmtStartFatfmtPatched, sceLflashFatfmtStartFatfmtOriginal);
                ClearCaches();
            }
    	}
        else if (strcmp(mod->modname, "scePower_Service") == 0)
        {
            REDIRECT_FUNCTION(FindImport("scePower_Service", "sceSyscon_driver", 0x65EB6096), sceSysconWriteScratchPadPatched);
            ClearCaches();
        }
        else if (strcmp(mod->modname, "sysconf_plugin_module") == 0)
        {
            void infinityVersionAppend(void);
            SceUID block_id = sceKernelAllocPartitionMemory(2, "", PSP_SMEM_Low, 25*4, NULL);

            if (block_id >= 0)
            {
                void *addr = sceKernelGetBlockHeadAddr(block_id);
                u32 func_addr = EXTRACT_J_ADDR(text_addr + 0x00019314) & ~0x80000000; // 6.61

                memcpy(addr, infinityVersionAppend, 25*4);
                _sw(func_addr, (u32)addr+3*4);

                MAKE_CALL(text_addr + 0x00019314, addr);
                ClearCaches();
            }
        }
	}

	return res;
}

int sceIoIoctlPatched(SceUID fd, u32 cmd, void *indata, int inlen, void *outdata, int outlen)
{
	// lets check for a few commands
	if (cmd == 0x208081)
	{
		// this command is checking the device, force return success (as to store a "non secure?" device in the exec_info)
		return 0;
	}

	// check for a loadmodule command but avoid the signcheck check
	else if ((cmd & 0x208080) == 0x208000)
	{
		// force a successful return (is checking if it's a loadmodule device, would return 0x80020146)
		return 0;
	}

	return sceIoIoctl(fd, cmd, indata, inlen, outdata, outlen);
}

void PatchModuleManager(void)
{
	SceModule2 *mod = sceKernelFindModuleByName("sceModuleManager");
    u32 text_addr = mod->text_addr;

    // patch call to PrologueModule from the StartModule function to allow a full coverage of loaded modules (even those without an entry point)
    KERNEL_HIJACK_FUNCTION(text_addr + 0x00008124, PrologueModulePatched, PrologueModule); //6.61
}

int SysEventHandler(int ev_id, char *ev_name, void *param, int *result)
{
    // on resume, reset the IPL decryption keys
    if (ev_id == 0x10009)
	{
		switch (sceKernelGetModel())
        {
            case PSP_MODEL_PHAT:
                memcpy((void *)0xBFC00200, g_661_1000_keys, sizeof(g_661_1000_keys));
                break;

            case PSP_MODEL_SLIM:
                memcpy((void *)0xBFC00200, g_661_2000_keys, sizeof(g_661_2000_keys));
                break;

            case PSP_MODEL_BRITE:
            case PSP_MODEL_BRITE4G:
            case PSP_MODEL_BRITE7G:
            case PSP_MODEL_BRITE9G:
                memcpy((void *)0xBFC00200, g_661_3000_keys, sizeof(g_661_3000_keys));
                break;

            case PSP_MODEL_PSPGO:
                memcpy((void *)0xBFC00200, g_661_go_keys, sizeof(g_661_go_keys));
                break;
        }
	}

	return 0;
}

PspSysEventHandler g_sysevent =
{
	sizeof(PspSysEventHandler),
	"",
	0x00FFFF00,
	SysEventHandler,
	0, 0, NULL,
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
};

int module_start(SceSize args, void *argp)
{
    kprintf("BEGIN 6.61 Control\n");

    // get the bootloader/core version from rebootex
    unsigned int (* _getCoreVersion)(void *set0, void *set0_) = (void *)REBOOTEX_ADDRESS;
    g_coreVersion = _getCoreVersion(NULL, NULL);

    PatchModuleManager();

    // register handler to fix sleep mode
	sceKernelRegisterSysEventHandler(&g_sysevent);

    ClearCaches();
	return 0;
}
