/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "reboot.h"
#include "utility.h"
#include "libinfinity.h"

#include <pspmacro.h>

#include <pspsysmem_kernel.h>
#include <pspinit.h>
#include <psploadcore.h>

#include <string.h>

int (* g_sony_reboot)(SceKernelBootParam *reboot_param, SceLoadExecInternalParam *exec_param, int api, int initial_rnd) = (void *)SONY_REBOOT_ADDRESS;

#ifdef LOAD_MS0
int (* g_rebootex_entry)(SceKernelBootParam *reboot_param, SceLoadExecInternalParam *exec_param, int api, int initial_rnd, int is_compat, int bootloader_version, char *infinityctrl, int size) = (void *)REBOOTEX_ADDRESS;
#else
int (* g_rebootex_entry)(SceKernelBootParam *reboot_param, SceLoadExecInternalParam *exec_param, int api, int initial_rnd, int is_compat, int bootloader_version) = (void *)REBOOTEX_ADDRESS;
#endif

char *g_rebootex = (char *)REBOOTEX_ADDRESS;
int g_rebootex_size = 0;
u32 g_sony_reboot_backup[2];

#ifdef LOAD_MS0
char *g_infinityctrl = (char *)0x88F10000;
int g_infinityctrl_size = 0;
#endif

void we_got_this_far()
{
    while(1)
    {
		REG32(0xbe24000c)  = 0xC0;
		Syscon_wait(250000/4);

		REG32(0xbe240008)  = 0xC0;
		Syscon_wait(250000/4);
    }
}

int onSonyRebootStart(SceKernelBootParam *reboot_param, SceLoadExecInternalParam *exec_param, int api, int initial_rnd)
{
#ifdef LOAD_MS0
    g_rebootex_entry(reboot_param, exec_param, api, initial_rnd, 0, g_coreVersion, g_infinityctrl, g_infinityctrl_size);
#else
    g_rebootex_entry(reboot_param, exec_param, api, initial_rnd, 0, g_coreVersion);
#endif

    memcpy((void *)SONY_REBOOT_ADDRESS, g_sony_reboot_backup, 8);
    ClearCaches();
    return g_sony_reboot(reboot_param, exec_param, api, initial_rnd);
}

int RebootStartPatched(SceKernelBootParam *reboot_param, SceLoadExecInternalParam *exec_param, int api, int initial_rnd)
{
    // need to patch AFTER custom firmware rebootex, if any
    memcpy(g_sony_reboot_backup, (void *)SONY_REBOOT_ADDRESS, 8);
    REDIRECT_FUNCTION(SONY_REBOOT_ADDRESS, onSonyRebootStart);
    ClearCaches();
    return 0;
}

int module_reboot_phase(int reboot_stage, void *reboot_param, u32 unk, u32 unk_2)
{
    // when kernel is about to be killed we do our patch
	if (reboot_stage == REBOOT_STAGE_PRE_KERNEL_KILLED)
	{
#ifdef LOAD_MS0
        //  read in our modules
        g_rebootex_size = ReadFile("ef0:/rebootex661.bin", 0, g_rebootex, 1*1024*1024);
        g_infinityctrl = g_rebootex+g_rebootex_size+0x10000;
        g_infinityctrl_size = ReadFile("ms0:/infinityctrl.prx", 0, g_infinityctrl, 1*1024*1024);

        if (g_infinityctrl_size < 0)
        {
            // just exit
            return 0;
        }
#else
        // read in from flash0
        g_rebootex_size = ReadFile("flash0:/kn/rebootex661.bin", 0, g_rebootex, 1*1024*1024);
#endif

        if (g_rebootex_size < 0)
        {
            // just exit
            return 0;
        }

        SceModule2 *mod = sceKernelFindModuleByName("sceLoadExec");

            // patch reboot in order to reload this module
        switch (sceKernelGetModel())
        {
            case PSP_MODEL_PHAT:
            case PSP_MODEL_SLIM:
            case PSP_MODEL_BRITE:
            case PSP_MODEL_BRITE4G:
            case PSP_MODEL_BRITE7G:
            case PSP_MODEL_BRITE9G:
            default:
                MAKE_CALL(mod->text_addr + 0x00002DAC - 0x18, RebootStartPatched); //6.61
                break;

            case PSP_MODEL_PSPGO:
                MAKE_CALL(mod->text_addr + 0x00002FF8 - 0x18, RebootStartPatched); //6.61
                break;

        }

        ClearCaches();
	}

	return 0;
}
