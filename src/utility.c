/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "utility.h"

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspiofilemgr.h>

#include <string.h>

int ReadFile(const char *file, int seek, void *buf, int size)
{
	SceUID fd = sceIoOpen(file, PSP_O_RDONLY, 0);
	if (fd < 0)
		return fd;

	if (seek > 0)
	{
		if (sceIoLseek(fd, seek, PSP_SEEK_SET) != seek)
		{
			sceIoClose(fd);
			return -1;
		}
	}

	int read = sceIoRead(fd, buf, size);

	sceIoClose(fd);
	return read;
}

int WriteFile(char *file, void *buf, int size)
{
	SceUID fd = sceIoOpen(file, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);

	if (fd < 0)
	{
		return fd;
	}

	int written = sceIoWrite(fd, buf, size);

	sceIoClose(fd);
	return written;
}


void ClearCaches(void)
{
#if _PSP_FW_VERSION > 631
	sceKernelIcacheInvalidateAll();
	sceKernelDcacheWritebackInvalidateAll();
#else
	sceKernelIcacheClearAll();
	sceKernelDcacheWritebackAll();
#endif
}

u32 FindExport(const char* modname, const char *lib, u32 nid)
{
	SceModule2 *mod = sceKernelFindModuleByName(modname);

	int i = 0, u;
	int entry_size = mod->ent_size;
	int entry_start = (int)mod->ent_top;

	while (i < entry_size)
	{
		SceLibraryEntryTable *entry = (SceLibraryEntryTable *)(entry_start + i);

		if (entry->libname && (strcmp(entry->libname, lib) == 0))
		{
			u32 *table = entry->entrytable;
			int total = entry->stubcount + entry->vstubcount;

			if (total > 0)
			{
				for (u = 0; u < total; u++)
				{
					if (table[u] == nid)
					{
						u32 func_addr = table[u + total];
						return func_addr;
					}
				}
			}
		}

		i += (entry->len * 4);
	}

	return 0;
}

u32 FindImport(const char *modname, const char *libname, u32 nid)
{
    u32 i = 0, u;

    /* find the module name */
    SceModule *mod = sceKernelFindModuleByName(modname);

    /* check if not found */
    if (!mod)
    {
        /* return no function */
        return 0;
    }

    /* get the stub information */
    u32 stub_start = (u32)mod->stub_top;
    u32 stub_size = mod->stub_size;

    /* if non of this info found, return no address */
    if (stub_start == 0 || stub_start == -1 || stub_size == 0)
    {
        /* no address */
        return 0;
    }

    /* loop until out of stubs */
    while (i < stub_size)
    {
        /* point the library */
        SceLibraryStubTable *stub = (SceLibraryStubTable *)(stub_start + i);

        /* if the libname exists, compare */
        if (stub->libname && strcmp(stub->libname, libname) == 0)
        {
            u32 *nidtable = (u32 *)stub->nidtable;

            /* loop through the nid table */
            for (u = 0; u < stub->stubcount; u++)
            {
                /* check for our nid */
                if (nidtable[u] == nid)
                {
                    /* force the function stub to jump to our new function, and return the pointer to the stub */
                    return (u32)&((u32 *)stub->stubtable)[u << 1];
                }
            }
        }

        /* increment the stub size times 4 */
        i += (stub->len << 2);
    }

    /* return no address */
    return 0;
}
