release: all
	psp-fixup-imports -m $(PSP_FW_VERSION)mapfile.txt $(TARGET).prx
	psp-packer -s 0x4C9494F0 0xDAEEDAEE $(TARGET).prx

TARGET = infinityctrl
OBJS = src/main.o src/flashfat.o src/reboot.o src/utility.o src/kprintf.o src/psp_uart.o src/sysreg.o src/syscon.o src/libinfinity.o src/infinity_ver.o src/sceSyscon_driver.o

GIT_VERSION := $(shell git rev-list HEAD --count)

INCDIR = include
CFLAGS = -std=c99 -Os -G0 -Wall -fno-pic -fshort-wchar -DGIT_COMMIT_VERSION=$(GIT_VERSION)
ASFLAGS = $(CFLAGS)

BUILD_PRX = 1
PRX_EXPORTS = src/exports.exp

PSP_FW_VERSION = 661

USE_KERNEL_LIBS=1
USE_KERNEL_LIBC=1

LIBDIR = lib
LDFLAGS = -nostartfiles
LIBS =

PSPSDK=$(shell psp-config --pspsdk-path)
include $(PSPSDK)/lib/build.mak
